Software design
================

.. uml::

   == Initialization ==

   Alice -> Bob: Authentication Request
   Bob --> Alice: Authentication Response

   == Repetition ==

   Alice -> Bob: Another authentication Request
   Alice <-- Bob: another authentication Response


.. plantuml::
   :caption: Caption with **bold** and *italic*

   Bob -> Alice: hello
   Alice -> Bob: hi


.. code-block:: plantuml

  Bob->Alice : hello
  Alice -> Bob : hi
